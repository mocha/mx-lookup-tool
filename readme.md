Look up MX servers and count them from a CSV of domains

### Requirements

1. Python 3? Would probably work with Python 2 but 🤷‍♀️

### Usage

1. Replace domains.csv with your own file
1. Run `python ./lookup.py`
1. ???
1. Profit

### Format

`domains.csv` is expecting a format of:

```
FQDN, Count
example.com,5
thing.com,1
...
```

I was processing a list that had a count of instances associated, so it made sense at the time.
