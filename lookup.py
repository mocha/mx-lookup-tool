#!/usr/bin/python3

import csv, os, re


# helper for logging
log = ""
def log_and_print(message):
    global log
    log = log + message + "\n"
    print(message)


# create a list of domains from the csv
domains = {}
with open('./domains.csv', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        if not row[0] == "FQDN":                                                            # ignore table header
            domains[row[0]] = { 'count': row[1] }                                           # write the dict


# function for looking up based on a domain
def lookup(domain):
    global domains
    
    log_and_print('Looking up ' + domain)
             
    stream = os.popen('host -W 1 -t MX ' + domain)                                          # set up stream
    result = stream.read()
    result = result.split('\n')[0]                                                          # grab only first line of output

    if not bool(re.match('.*not found.*', result)):                                         # filter out domain failures

        if not bool(re.match('.*no MX.*', result)):                                         # filter out no mx failures

            result = re.sub(domain + r' mail is handled by \d* ', '', result)               # remove cruft
            result = result.lower()                                                         # lowercase it
            result = result.rstrip('.')                                                     # remove trailing dot

            log_and_print("  ⟶  " + result)                                                # log for posterity
            domains[domain]['mx_server'] = result                                           # drop in domains dict
            return result

        else:
            log_and_print(" ❌  No MX server specified")

    else:
        log_and_print(" ❌  Domain not found")


# look up the list, dump in to a dict
domains_count = len(domains)
current_domain_no = 1                                                                       # log ticker
for key in domains.keys():
    print("\n(" + str(current_domain_no) + " / " + str(domains_count) + ")")                  # log ticker
    current_domain_no = current_domain_no + 1                                               # log ticker
    lookup(key)                                                                             # do the lookup!


# write the DOMAINS output file from the dict
with open('output-domains.csv', 'w') as output_file:                                        # open new file
    output_file.write("Domain,Count,MX Server\n")
    for key in domains.keys():
        if 'mx_server' in domains[key]:                                                     # sanity check there's a server there
            output_file.write(
                "%s,%s,%s\n"%(key,domains[key]['count'],domains[key]['mx_server'])          # scribble scribble
            )
    log_and_print("\n💾 Saved output-domains.csv")


# build the mx server dict
mx_servers = {}
for key in domains.keys():
    try:
        server = re.match(r'(.*\.)(.*\..*)$', domains[key]['mx_server']).group(2)           # parse subdomain off

        if server in mx_servers.keys():                                                     # if it's already there...
            mx_servers[server] = mx_servers[server] + int(domains[key]['count'])            # add existing + new counts
        else:
            mx_servers[server] = int(domains[key]['count'])
        
    except:
        log_and_print(                                                                      # catch rando errors
            "❌  Whoops? " + "domain \"" + key + "\" didn't work.")


# write the SERVERS output file from the dict
with open('output-servers.csv', 'w') as output_file:
    output_file.write("MX Server,Count\n")
    for key in mx_servers.keys():
        output_file.write("%s,%s\n"%(key,mx_servers[key]))                                  # scribble scribble
    log_and_print("\n💾  Saved output-servers.csv")

# output log
with open('./log.txt', 'w') as file:
    file.write(log)                                                                         # write the log
    log_and_print("\n💾  Saved log.txt")
